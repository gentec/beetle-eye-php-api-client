<?php

namespace BeetleEye;

class Client {

    /**
     * 
     * submit type of  sales
     */
    const TYPE_SALES = "sales";

    /**
     * submit type of form
     */
    const TYPE_FORMS = "forms";

    /**
     * call modes
     */
    const CALL_MODE_DEVELOPMENT = "development";
    const CALL_MODE_PRODUCTION = "production";

    /**
     * The variable should hold the api key for making requests to the Beetle Eye
     *
     * @var string
     */
    private $apiKey = "";

    /**
     * The variable should hold the type of data to be sent to the Beetle Eye
     *
     * @var string
     */
    private $submissionType = "";

    /**
     * The variable should hold the key to the form that is about to be send.
     *
     * @var string
     */
    private $formKey = "";
    
    /**
     * The variable should hold the identifier to the sale that is about to be send.
     *
     * @var string
     */
    private $sale_id = "";

    /**
     * The variable should hold the source
     *
     * @var string
     */
    private $source = "";

    /**
     * The variable should hold the ssl verification FLAG
     * 
     * @var boolean
     */
    private $ssl_verification = TRUE;

     /**
     * The variable should hold the SAVE IP FLAG
     * 
     * @var boolean
     */
    private $SAVE_IP = TRUE;

    /**
     * The variable should hold the call request type as production or development 
     *
     * @var string
     */
    private $call_mode = "";

    /**
     * The variable should hold the group identifier.
     *
     * @var string
     */
    private $group = "";

    /**
     * The variable should hold the tracking cookie key if exist
     * 
     * @var string
     */
    private $cookie_key = NULL;

    /**
     * The variable should hold the possible Beetle Eye API's endpoints for communication
     *
     * @var array URLs
     */
    private $endpoints = array(
        "forms" => "https://app.beetle-eye.com/api/v1/add-form",
        "sales" => "https://app.beetle-eye.com/api/v1/add-sale"
    );

    /**
     * The variable should hold the Beetle Eye API's endpoint for communication
     *
     * @var string URL
     */
    private $endpoint = "";

    /**
     * Creates a new Beetle Eye Client Instance
     *
     * @param string Beetle Eye Api Key
     * @param string Source (URL)
     */
    public function __construct($apiKey = "", $source = "") {
        $this->apiKey = $apiKey;
        $this->source = $source;
        $this->call_mode = self::CALL_MODE_PRODUCTION;

        if (isset($_COOKIE['_beTracking'])) {
            $cookie_val = json_decode($_COOKIE['_beTracking']);
            if (isset($cookie_val->key)) {
                $this->cookie_key = $cookie_val->key;
            }
        }
    }

    /**
     * Dynamically change the ssl verification for requests
     * 
     * @param boolean SSL Verification
     * @return void
     */
    public function setSslVerification($verify) {

        $this->ssl_verification = $verify;

    }

     /**
     * Dynamically change the SAVE IP for requests
     * 
     * @param boolean SAVE IP
     * @return void
     */
    public function setSaveIP($save_ip) {

        $this->SAVE_IP = $save_ip;

    }

    /**
     * Dynamically changes the Beetle Eye Api Key, should it be necessary.
     *
     * @param string Beetle Eye Api Key
     * @return void
     */
    public function setApiKey($apiKey = "") {
        $this->apiKey = $apiKey;
    }

    /**
     * Sets the data submission type
     *
     * @param string submissionType
     * 
     * valid values - forms | sales
     * @return void
     */
    public function setSubmissionType($submissionType = "") {
        $this->submissionType = $submissionType;
        $this->endpoint = array_key_exists($submissionType, $this->endpoints) ? $this->endpoints[$submissionType] : "";
    }

    /**
     * Dynamically changes the Source, should it be necessary.
     *
     * @param string Source (URL)
     * @return void
     */
    public function setSource($source = "") {
        $this->source = $source;
    }

    /**
     * Dynamically changes the call mode.
     *
     * @param string production/development
     * @return void
     */
    public function setCallMode($call_mode = "production") {
        $this->call_mode = $call_mode;
    }

    /**
     * Dynamically changes the group identifier.
     *
     * @param string
     * @return void
     */
    public function setGroup($group = "") {
        $this->group = $group;
    }

    /**
     * Dynamically sets the Form Key, should the data submission type be of type forms.
     *
     * @param string formKey 
     * @return void
     */
    public function setFormKey($formKey = "") {
        $this->formKey = $formKey;
    }
    
    /**
     * Dynamically sets the Sale identifier, should the data submission type be of type sales.
     *
     * @param string sale_id 
     * @return void
     */
    public function setSaleId($sale_id = "") {
        $this->sale_id = $sale_id;
    }

    /**
     * Sends data to the Beetle Eye
     *
     * @param array data
     * @param array lead - for sales
     * @return array
     *
     * Valid keys in the returned array are 'error' and 'success'
     */
    public function sendData($data, $lead = array()) {
        // check for empty required fields and return with the error, if any.
        $emptyFields = $this->areFieldsFilled();
        if (array_key_exists("error", $emptyFields)) {
            return $emptyFields;
        }

        $postData = array('api_key' => $this->apiKey,
            'source' => $this->source,
            'call_mode' => $this->call_mode,
            'cookie_key' => $this->cookie_key,
            'group' => $this->group,
            'ip' => $this->getIPAddress(),
            'save_ip' => $this->SAVE_IP,
            'post_url' => $this->getFullURL());

        if ($this->submissionType == self::TYPE_FORMS) {
            $postData = array_merge($postData, array('form_key' => $this->formKey, 'data' => $data));
        } else if ($this->submissionType == self::TYPE_SALES) {
            $postData = array_merge($postData, array(
                'sale_id' => $this->sale_id,
                'lead' => $lead,
                'data' => $data));
        }
        $headers = array();
        $request = \Requests::post($this->endpoint, $headers, $postData, $this->getOptions());
        // fetch the response and check for errors

        $responseBody = json_decode($request->body, true);

        // malformed response has been encountered
        if (!is_array($responseBody) || !count($responseBody)) {
            return array("error" => "An unknown error occurred");
        }

        return $responseBody;
    }

    /**
     * Checks which required fields for submitting data are missing and returns them in an array with key 'error'
     *
     * @return array
     *
     * Valid keys in the returned array are 'error' and 'success'
     */
    private function areFieldsFilled() {
        $errors = array();
        if (!$this->apiKey) {
            // notify that an API Key has not been set
            array_push($errors, "Missing API Key");
        }

        if (!$this->endpoint) {
            array_push($errors, "You need to call the setSubmissionType method with a valid submission type to set up the data that you want to submit before you can send data.");
        }

        if (!$this->formKey && $this->submissionType && $this->submissionType === self::TYPE_FORMS) {
            // notify that an API Key has not been set
            array_push($errors, "Missing Form Key");
        }
        
        if (!$this->sale_id && $this->submissionType && $this->submissionType === self::TYPE_SALES) {
            // notify that an API Key has not been set
            array_push($errors, "Missing Sale ID");
        }
        
        if (!$this->source) {
            // notify that an API Key has not been set
            array_push($errors, "Missing Source");
        }
        if (!count($errors)) {
            return array("success" => true);
        }
        return array("error" => $errors);
    }

    /**
     * Collect all available options for request
     * 
     * @return array
     */
    private function getOptions() {
        return array(
            "verify" => $this->ssl_verification
        );
    }

    /**
     * 
     * Get IP address
     * 
     * @return string
     */
    private function getIPAddress() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        } else if (isset($_SERVER['REMOTE_ADDR'])) {
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        } else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else if (isset($_SERVER['HTTP_X_FORWARDED'])) {
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        } else if (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        } else if (isset($_SERVER['HTTP_FORWARDED'])) {
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        } else {
            $ipaddress = '';
        }
        return $ipaddress;
    }

    /**
     *  
     * @return string
     */
    private function getFullURL() {
        return (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    }

}
