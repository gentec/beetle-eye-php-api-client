# Beetle Eye PHP Client

The *Beetle Eye PHP Client* is a library that allows you to easily make requests to the Beetle Eye API in order to submit arbitrary data which will be stored in Beetle Eye. At the moment, we allow the submission of forms and sales. Storing forms or sales in Beetle Eye would allow you to get an insight of the submissions such as statistics or/and use the data for different purposes such as mass emails.

## How to use it?


### Instantiating the Client and setting up the required fields

First, you would have to *install* the `\BeetleEye\Client` using composer and add your composer dependencies using `require_once("vendor/autoload.php");`


When instantiating `\BeetleEye\Client` you can pass your API key and the source in that specific order. 
```php

$beetle_eye = new \BeetleEye\Client("my-secret-api-key", "[SOURCE ID]");

```
Alternatively, you can set up the API key, the source ID and remove SSL Verification after instantiation.

```php

$beetle_eye = new \BeetleEye\Client();
$beetle_eye->setApiKey("[YOUR_API_KEY_HERE]");
$beetle_eye->setSource( "[SOURCE ID]");
$beetle_eye->setCallMode(\BeetleEye\Client::CALL_MODE_PRODUCTION);
$beetle_eye->setSslVerification(FALSE);
$beetle_eye->setSaveIP(TRUE);
```

### Sending forms

Thereafter, to send a form, you can write:

```php

$beetle_eye->setFormKey("[FORM KEY]");
$beetle_eye->setSubmissionType(\BeetleEye\Client::TYPE_FORMS);
var_dump($beetle_eye->sendData(["first_name" => "Fname", "email" => 'test@test.com']));   
```

What we did here is set up the form key for the form submission that we want to store and told the Beetle Eye Client that we are going to send a form. Finally, we send two key/value pairs representing the form data.

### Sending sales

To send a sale, you just have to tell the Client that you are going to send sales, and pass your sales data to the `sendData` method.

```php
$beetle_eye->setSubmissionType(\BeetleEye\Client::TYPE_SALES);
$beetle_eye->setSaleId('123456-unique');
$sales = [
    ['total_cost_with_tax' => 99.99, 'cc_name' => 'VISA'],
    ['total_cost_with_tax' => 24.99, 'cc_name' => 'VISA'],
];
$lead = ['first_name' => 'Fname', 'last_name' => 'Lname', 'email' => 'test@test.com'];
var_dump($beetle_eye->sendData($sales, $lead));

```

Note here that you can send any field from the eSDL list.

### Thorough example


So, starting your file from scratch, you can send both a form and a sale by using the code below:

```php

require_once("vendor/autoload.php");
$beetle_eye = new \BeetleEye\Client();
$beetle_eye->setApiKey("[YOUR_API_KEY_HERE]");
$beetle_eye->setSource( "[SOURCE ID]");
$beetle_eye->setCallMode(\BeetleEye\Client::CALL_MODE_PRODUCTION);
$beetle_eye->setFormKey("[FORM KEY]");
$beetle_eye->setSubmissionType(\BeetleEye\Client::TYPE_FORMS);
var_dump($beetle_eye->sendData(["first_name" => "Fname", "email" => 'test@test.com']));

$beetle_eye->setSubmissionType(\BeetleEye\Client::TYPE_SALES);
$beetle_eye->setSaleId('123456-unique');
$sales = [
    ['total_cost_with_tax' => 99.99, 'cc_name' => 'VISA'],
    ['total_cost_with_tax' => 24.99, 'cc_name' => 'VISA'],
];
$lead = ['first_name' => 'Fname', 'last_name' => 'Lname', 'email' => 'test@test.com'];
var_dump($beetle_eye->sendData($sales, $lead));

```

Note that the sales submission type requires a specific data structure which would be shown to you if you omit a required field for it.

You can use the `setApiKey`, `setFormKey`, `setSource`, `setSslVerification` and `setSaveIP` whenever needed to make requests with a different setting.


